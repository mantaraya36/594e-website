Title: Final Projects
Date: 2015-06-15

## Janice Agustin, Too Close


Too Close is an installation project using Arduino, muscle wires, and photocells to create moving paper. 

How it works? 
Current passing through the wires activate the muscle wires attached to my handmade Abacá paper. I’m using photocells, which are sensors that detect light, to tell the muscle wires to move. There are four areas of the sculpture where I’ve placed photocells facing up and outwards where they can easily detect light and light changes. When a viewer accidentally or intentionally hovers over the photocells, their cast shadows will activate the muscle wires to respond in life-like movements. The tension and resistance between the papers and muscle wires cause only subtle rather than abrupt and obvious movement.

[Too Close.pdf](|filename|/projects/JaniceAugustin/Too Close.pdf)

[too_close.ino](|filename|/projects/JaniceAugustin/too_close.ino)


## Alexander Champlin


[final_start.pyde](|filename|/projects/AlexanderChamplin/final_start.pyde)

[pylasttest2.csv](|filename|/projects/AlexanderChamplin/pylasttest2.csv)

[pylasttest2.py](|filename|/projects/AlexanderChamplin/pylasttest2.py)

[pylasttest3.csv](|filename|/projects/AlexanderChamplin/pylasttest3.csv)



## Lisa Han, Timeline for #BringBackOurGirls


This data visualization presents two dimensional timeline array based on a database of user information collated form the Twitter API. Specifically, its goal is to visualize influencers within the hashtag #BringBackOurGirls for roughly 100 users. The influencer in this case is defined by their popularity (number followers) and quantity of tweets over their lifetime. Popularity is represented on the y axis, with the origin at the top left corner, while tweet quanity is represented by the size of the ellipse in the array. I added a temporary metric called “username type” and correlated it with color to represent the possibility of a third qualitative metric to describe users. In this case, username type refers to whether or not the username contains letters, a mixture of letters and numbers, or a mixture of letters and other characters. When clicked, the timeline displays the usernames for each of the users, represented by a circle and changes the transparency of the cicles. While the correlation between tweeter volume and oorder is not strong, this demo does show that there is a significant correlation between popularity and tweet volume. 

In terms of code, I used java to create two separate arrays of two objects defined by their own classes: textbox and tweetellipse. Using a loadtable function, I was able to retrieve all the information from a CSV file. I then implemented the color metric in the setup function, and initialized the ellipses as well as the text and added instances of them to their own arrays. For the class tweetellipse I floated the coordinates and width and height of the ellipse, and for the textbox I simply replaced the geometric parametrs with a string. The draw function is where I added most of the functionality by creating a “for” statement that would iterate through all the data and create ellipses from the class. I then added a mousepressed functionality with the textbox array and change the opaque ellipses into transparent ellipses. As a final touch, I created two lines that would follow the mouse around the array. 

A later version of this template will be to go beyond one data set to compare different hashtags based on levels on the y axis. This will allow me to see purely the information about influencers and compare the hashtag sets directly to each other. I plan to modify the metrics to make ellipse size correlate to popularity and tweet volume correlate to a color heat map. I believe this will present a clearer correlation, as it is more likely the number of followers and not the number of tweets that determines the influence of the user. I also plan to embellish the interactivity to display additional information for each data point, including username and perhaps text clouds displaying tweet content for each hashtag.

[bringbackourgirls1.csv](|filename|/projects/LisaHan/bringbackourgirls1.csv)

[lisa_han_final.pde](|filename|/projects/LisaHan/lisa_han_final.pde)

[Lisa.Han_final.docx](|filename|/projects/LisaHan/Lisa.Han_final.docx)



## Thong Win, Clip Archive


This project is a collaboration with the Department of Film and Media Studies Clip Archive project. This python code accesses a database of film and television clips commonly used in core classes. Users are able to choose between the two databases and search for specific texts. By inputting the unique key for any clip, the program pulls up the clip in the web browser. The program streamlines class instruction by allowing instructors to use clips in class without queuing DVDs and VHS tapes. Likewise the program also allows students to do close analysis on key scenes at home.

[ClipArchive00test.csv](|filename|/projects/ThongWin/ClipArchive00test.csv)

[final.py](|filename|/projects/ThongWin/final.py)

[tbl_TVXXXXtest2.csv](|filename|/projects/ThongWin/tbl_TVXXXXtest2.csv)



## Rene Mireles, Color Keyboard


The color keyboard takes input from an audio file and turns it into a visual display based on different variables.  In addition, the user can press different keys to change the display color and shapes that are shown.  To print the screen, the user just needs to print a non alphabetical key and it is saved as a “ScreenCapture” png file. In the future, I hope to use this program to make a synthesizer that also creates different color patterns depending on the user’s input.

[colorkeyboard.pde](|filename|/projects/ReneMireles/colorkeyboard.pde)

[jingle.mp3](|filename|/projects/ReneMireles/jingle.mp3)
