Title: Workshop 1
Date: 2015-04-02

Write a Python program that analyzes a color in RGB hex format (e.g. 0xff0011) and says something like red blueish. To do this, you need to separate the three components from the RGB representation using bitwise operations (bit masking and bit shifting) and then you need to compare the values. Whichever is greater determines the main color and the second greatest one determines the "-ish" part. Also cover these (and possibly other) corner cases:

 * All values are the same then call it "gray", "white" or "black" depending on the values (or maybe "dark gray" or "light gray").
 * If two of the colors are equal choose a solution both for when they are the highest value as well as when they are the lowest.
 * If only one of the colors has a value and the others are zero, call it "dark red", "bright red", etc.

<!-- Additional Readings: -->
<!-- -------------------- -->
