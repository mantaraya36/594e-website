Title: Workshop 4
Date: 2015-04-23

Write, build and run a C program that requests a number from the user and then prints "Your number is 5!".

To do this you will need:

* To set up your C environment. You will need at least a compiler, and we suggest you get an IDE like QtCreator. If you want to try others, please do so.
* To write your C program using the C standard library "stdio". Include the header in your code.
* This library provides functions to print to and get input from the terminal:
```
int number;
scanf("%i", &number);
```
* Build and run your program


<!-- Additional Readings: -->
<!-- -------------------- -->
