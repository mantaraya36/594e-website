Title: Final Projects
Date: 2015-12-22


## Woohun Joo

1. Color Driven Pattern Music

[Joo1.png](|filename|/projects_F2015/WoohunJoo/Joo1.png)

As a series of my previous platform of color-sound conversion, I continue to experiment with colors and geometrics as the basis for electronic music composition and sound synthesis. For this final project, I make a piece of color driven music combining with visuals that presents the color scanning process. Hue values are converting to particular range of sound frequencies and each pixel unit (the screen width is divided by 512.) serves as a sound making elements for additive synthesis.

https://vimeo.com/148756411


2. Sound responsive album cover 

[Joo2.png](|filename|/projects_F2015/WoohunJoo/Joo2.png)

Each graphic element is moving according to the sound level(volume). 
The cover design is taken from Wolfmother’s Victorious single.

[processingsketch.zip](|filename|/projects_F2015/WoohunJoo/processingsketch.zip)



## Lu Liu 

[Liu1.png](|filename|/projects_F2015/LuLiu/Liu1.png)
[Liu2.png](|filename|/projects_F2015/LuLiu/Liu2.png)
[Liu3.png](|filename|/projects_F2015/LuLiu/Liu3.png)

Code
[sketch_151209a.zip](|filename|/projects_F2015/LuLiu/sketch_151209a.zip)



## Junxiang Yao

[Yao1.png](|filename|/projects_F2015/JunxiangYao/Yao1.png)
[Yao2.png](|filename|/projects_F2015/JunxiangYao/Yao2.png)
[Yao3.png](|filename|/projects_F2015/JunxiangYao/Yao3.png)
[Yao4.png](|filename|/projects_F2015/JunxiangYao/Yao4.png)
[Yao5.png](|filename|/projects_F2015/JunxiangYao/Yao5.png)
[Yao6.png](|filename|/projects_F2015/JunxiangYao/Yao6.png)

Code
[Fibonaci_galaxy.zip](|filename|/projects_F2015/JunxiangYao/Fibonaci_galaxy.zip)



## Jing Yan





## Jacob Burrows

This is my first project in csound and cabbage. It has three parameters that are controlled by sliders. This is the Jverb or Jacob's Reverb.

[Burrows.png](|filename|/projects_F2015/JacobBurrows/Burrows.png)

Plugin
[Jverb.vst](|filename|/projects_F2015/JacobBurrows/Jverb.vst)




