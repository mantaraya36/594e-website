Title: Homework 2
Date: 2015-04-17

*Due: Thursday, April 30th*

Write *one* of these two python programs that can compile and dissassemble programs for the fictional microcontroller designed in class.

Compiler
-------
The compiler should be a able to read a file like:
```
0x0D 0x04
0x0E 0x05
----
ADD 0x0D
ADD 0x0E
```

And turn it into a text file that contains:
```
0D0E0000000000000000000000040500
```
Notice how the commands are placed at the initial memory locations and the lines that are not commands determine the initial contents of memory. The original file is separated in two parts, marked by a line with four '-' characters. The first part sets memory values directly, and the second part is the "program".

This compiler doesn't have to be super robust, i.e. it should not need to handle errors in the file, but it should compile any valid "program".

Dissassembler
-----

The dissassembler should read a text file containg the memory layout for the microporcessor, like:
```
0D0E0000000000000000000000040500
```

And turn it into the original code:
```
0x0D 0x04
0x0E 0x05
----
ADD 0x0D
ADD 0x0E
```

To do this, it must first get the program instructions starting at the beginning and after an end instruction is reached, any non-zero values should be set in the initial part of the code.


Tips
----
To convert hexadecimal numbers in a string to an integer use the *int()* function:
```
>>> int("0x0f", 0)
15
```
To find an element in a list:
```
l.index('text')
```
Iteration:
```
memory = '0D0E0000000000000000000000040500'
for p in range(16):
    print memory[p*2:] # This is not really what you need, but it's close
```
    


Name your file: hw2-NAME_SURNAME.py (substitute NAME and SURNAME by your own, notice that there are no spaces) and send by email to both the lecturer and TA.
