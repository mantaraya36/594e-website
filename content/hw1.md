Title: Homework 1
Date: 2015-04-08

*Due: Thursday, April 16th*

Make a python program that requests a number and then prints the multiplication table for that number. The program should look like:

```
$ python hw1.py
Enter a number: 5
5 x 1 = 5
5 x 2 = 10
5 x 3 = 15
5 x 4 = 20
5 x 5 = 25
5 x 6 = 30

... etc

```

You can request a number with the code:

```
number = input("Enter a number: ")
```

Make the printing of the table a function that takes a number:

```
def print_table(number):
     .... code here
```

You can create the text for each line in different ways, for example:
```
# The comma character at the end avoids moving to the next line
print num,
print " x ",
print i,
print " = ",
print i*num # no comma here
```
You can also construct strings of characters, but you need to turn numbers into strings to be able to concatenate:

```
line = str(num) + ' x ' + str(i) + ' = ' + str(num*i)
print line
```

Or you can use string formatting:

```
line = "%i x %i = %i"%(num, i, num*i)
print line
```

Name your file: hw1-NAME_SURNAME.py (substitute NAME and SURNAME by your own, notice that there are no spaces) and send by email to both the lecturer and TA.
