Title: Workshop 5
Date: 2015-04-30

Write, build and run a C program that calculates the [Fibonacci sequence](http://en.wikipedia.org/wiki/Fibonacci_number), and then the [Golden Ratio](http://en.wikipedia.org/wiki/Golden_ratio) from the last terms.

The program should first ask the user how many Fibonnaci numbers to calculate. Then it should print those numbers, and finally should print the approximation of the golden ratio from the last two.

To do this you will need:

* To use *scanf()* to get user input
* A *for* loop to generate the numbers. Remember that the *for* loop syntax in C is:
```
for (int i = 0; i < 10; i++) {
 /* Whatever you need to iterate over goes here */
}
```
* Have a mechanism of keeping the last two values from the sequence to then calculate the golden ratio
* Be careful with integer division!

