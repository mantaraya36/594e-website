Title: Workshop 7
Date: 2015-05-28

Bingo Machine
-------------

Create a class in Python that can play Bingo. A program that uses the class should look like:

```python
b = BingoMachine(4) # The argument is the number of players
b.showBoard(0) # This prints the boards generated for the players (showing with an X the numbers that have been called

b.getNumber() # This function should print who has the number and whether a player has filled the board

b.playAll() # This function will choose numbers until a player has won the game

b.restart() # The boards will be regenerated 
```

Some notes:

* The constructor for the *BingoMachine* class needs to generate the boards and store them in member variables
* All the random generators in this problem (both to create the boards as well as when playing must draw from a set which . You can use the [sample() function from the random module](https://docs.python.org/2/library/random.html#random.sample) or you can get random elements from a [set](https://docs.python.org/2/library/stdtypes.html#set) using its *pop()* function. In the latter case you will need to shuffle the elements into the set to make sure the order is different every time.
* You can store the boards in nested lists (to simulate a matrix) and you can choose an invalid value like -1 or 0 to indicate the number has been called.
* Try to organize your class in a way that function are never too long and unwieldy


