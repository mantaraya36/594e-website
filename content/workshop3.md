Title: Workshop 3
Date: 2015-04-16

Write a Python program that emulates the fictional microcontroller designed in class.

The program should:

1. Read a text file containing the machine language "code"
2. Copy the contents of this file with the right conversion into the emulated memory registers
3. Run the program by processing the commands until an END instruction is reached.
4. Print the value in the accumulator register

Microcontroller architecture
--------
 * 16 8 bit memory registers named 0x00 -> 0x0F
 * The accumulator is located in address 0x0F
 * Program execution starts at memory 0x00
 * All commands consist of one byte, the most significant nibble (4 bits) is the command, the least significant nibble is the command data

Commands
-------

* 0x0X: ADD - add contents of address 0x0X to accumulator overwriting it. 
* 0x1X: SUB - subtract contents of  address 0x0X to accumulator overwriting it. 
* 0x2X: LOAD - copy contents of address 0x0X to accumulator overwriting it. 
* 0x3X: SAVE - copy contents of accumulator to address 0x0X.
* 0x4X: END - End execution

<!-- Additional Readings: -->
<!-- -------------------- -->
