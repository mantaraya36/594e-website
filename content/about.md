Title: Info
Date: 2015-01-05
Author: Andres Cabrera
Category: Information

*Lecturer:* Andrés Cabrera <andres@mat.ucsb.edu>

*Room:* Elings 2003

*TA:* Nathan Weitzner

*Quarter:* Spring 2015

*Mailing list:* [http://lists.create.ucsb.edu/mailman/listinfo/594e](http://lists.create.ucsb.edu/mailman/listinfo/594e)

Summary
===========================================
This course presents an introduction into the concepts, tools and techniques related to programming and computing for users who don’t have a technical background.

Most books and courses dedicated to programming and computing often assume the learner has
a technical background and an understanding of some of the fundamental underlying processes
and systems. Additionally, the material is geared to specific disciplines and goals that are often very different to those required for the arts and the humanities. This course seeks to bridge this gap by focusing on practical tools that are relevant and useful in the field of arts, humanities and the social sciences.

Unlike other courses that teach domain specific languages like Processing or Arduino to the
same target audience, this course seeks to establish a strong foundation upon which the learning and understanding of these higher-level languages will be greatly simplified. As highly practical course, it hopes to get students started with programming together with a clear understanding of the processes involved to be able to know what’s going on when things go wrong.

[Syllabus](|filename|/res/594E_Syllabus.pdf)





