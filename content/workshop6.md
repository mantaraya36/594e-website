Title: Workshop 6
Date: 2015-05-07

Select a Website that provides a web API, and find a python module that assists in getting the data from the website. Install the module and get some data from the page. Then do one or more statistical analysis on the data, e.g.:

* count words
* find average numbers for some field
* sort by geographic location, year, month, or any other field
* plot data (you can use python or another tool like Excel)

