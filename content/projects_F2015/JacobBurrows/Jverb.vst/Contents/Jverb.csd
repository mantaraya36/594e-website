<Cabbage>
;                           lgth  hgt
form caption("Reverb"), size(600, 400), colour(700, 5, 6,) pluginID("jaco")
;                                               horz vert lgth hght
groupbox text("That Cave Effect Though"), bounds(55, 100, 500, 200), colour(50, 800, 350)
hslider channel("FATNESS"), bounds(80, 120, 455, 70), text("FATNESS"), range(0, 1, 0.2)
hslider channel("CUTOFF"), bounds(85, 170, 450, 75), text("CUTOFF"), range(0, 22000, 10000)
hslider channel("LOUDNESS"), bounds(70, 228, 465, 70), text("LOUDNESS"), range(0, 1, 0.5)
</Cabbage>
<CsoundSynthesizer>
<CsOptions>
;-d=suppress graphics -n stop writing sound to output
-d -n
</CsOptions>
<CsInstruments>

sr = 44100	
;samples per sec
ksmps = 64	
nchnls = 2			
0dbfs = 1			
;retreives the string value
instr 1 
;k=rate
	kFDBack chnget "FATNESS"
	kFco chnget "CUTOFF"
	kGain chnget "LOUDNESS"
	aInL inch 1
	aInR inch 2
	aOutL, aOutR reverbsc aInL, aInR, kFDBack, kFco 
	outs aOutL*kGain, aOutR*kGain
endin

</CsInstruments>
<CsScore>
; srt dur rq  harm of the wave form
f1 0 4096 10 1
; strt dur of instrument
i1 0 1000
</CsScore>
</CsoundSynthesizer>