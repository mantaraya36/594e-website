class Ball{
  PVector position;
  PVector velocity;
  boolean is_display;
  float r;

  Ball(float x, float y, float r){
    position = new PVector(x,y);
    velocity = new PVector(random(-2.0, 2.0 ),random(-2.0,2.0));
    this.r =r;
    is_display=false;
  }
  
  void setPos(float x, float y){
    position.x=x;
    position.y=y;
    is_display=true;
  }

  void move(){
    velocity.add(gravity);
    position.add(velocity);
  }
  
  void display(){
    //if(is_display){
    stroke(random(100,200),125);
    fill(200,125);
    ellipse(position.x,position.y, r*2, r*2);
    //}
  }
  
  
  boolean is_collide(float x2, float y2, float r2)
  {
    float x1 = this.position.x;
    float y1 = this.position.y;
    float r1 = this.r;
    float distsquared = (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2);
    if(distsquared < 0.6 * (r1 + r2)*(r1 + r2) )
     return true;
    else
     return false;
  }
 }

  