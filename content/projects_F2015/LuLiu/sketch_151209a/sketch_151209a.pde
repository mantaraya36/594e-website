import processing.sound.*;
// sound library
SoundFile file;

PImage bg;
PImage img;
// background image

Ball ball;
Star star;
Star starCenter;

ArrayList<Ball> balllist;
ArrayList<Star> starlist;

PVector gravity = new PVector(0,0);

void setup(){
  size(600,450, P2D);
  
noStroke();
 smooth();
// textures();
//textureMode(NORMALIZED);
  
  
  file = new SoundFile(this, "starsky.mp3");
  file.play();
  
  //spotLight(0,255, 255, 80, 20, 40, -1, 0, 0, PI/2, 2);

  bg = loadImage("star.jpg");
  img = loadImage("sun.png");
  balllist= new ArrayList<Ball> ();
  starlist = new ArrayList<Star> ();

  
  float axis1 = 210;
  float speed1 = 0.005;
  starCenter = new Star(300,225,50,50, 0, 0,0);
  //set starlist size=4
  for(int i=0; i<5; i++)
  { 
    star = new Star(0,0,random(20,30),10, axis1, axis1-20,speed1);
    starlist.add(star);
    axis1 -= 30;
    speed1 += 0.005;
  }
}

void mousePressed(){
  ball = new Ball(30,30,5);
  ball.setPos(mouseX,mouseY);
  balllist.add(ball);
}


void draw(){
  background(bg);
 
  //ellipseMode(CENTER);
  //ellipse(300,225,50,50);
 //  starCenter.display();
 noStroke();
 beginShape();
 texture(img);
 vertex(270, 195, 0, 0);
 vertex(330, 195, 240, 0);
 vertex(330, 255, 240, 240);
 vertex(270, 255, 0, 240);
 endShape(CLOSE);
 
  for(int i=0;i<starlist.size();i++)
  { 
    Star star = starlist.get(i);
    star.display();
  }
  
   for(int i=0; i<balllist.size(); i++){
  Ball ball1= balllist.get(i);
  ball1.move();
  ball1.display();
  }
  
  for(int i=0;i<balllist.size();i++)
  {
    Ball ball = balllist.get(i);
    float x1  = starCenter.position.x;
    float y1  = starCenter.position.y;
    float r   = starCenter.r1;
    if(ball.is_collide(x1,y1,r))
    {
       float v2x = ball.velocity.x;
       float v2y = ball.velocity.y; 
       ball.velocity.x = -v2x;
       ball.velocity.y = -v2y;
    }
  }
  
  for(int i=0; i<balllist.size();i++)
  {
    for(int j=0;j<starlist.size();j++)
    {
      Ball ball = balllist.get(i);
      Star star = starlist.get(j);
      float x1  = star.position.x;
      float y1  = star.position.y;
      float r   = star.r1;
      if(ball.is_collide(x1, y1, r))
      {
       //PVector velocity_old = ball.velocity;
       //ball.velocity.x = velocity_old.x * -0.8;
       //ball.velocity.y = velocity_old.y * -0.8;
       float v2x = ball.velocity.x;
       float v2y = ball.velocity.y;
       float v1x = star.vx;
       float v1y = star.vy;
       //calculate relative velocity
        v2x = 2 * v1x - v2x;
        v2y = 2 * v1y - v2y;
        ball.velocity.x = v2x;
        ball.velocity.y = v2y;
      }
    }
  }
 
 
}