class Star{
//cos motion -> constant + cos(angle) * scalar. Sin motion is the same.
// Sin motion + cos motion = Circular motion
int constant1 = 300;
int constant2 = 225;
float angle = 0.03;
float scalar1; //= random(90,100);
float scalar2; //= scalar1-30;
float speed = random(0.01, 0.05);
float vx;    //the x component of velocity for star
float vy;    //the y component of velocity for star

 PVector position;
 float r1;
 float r2;

 Star(float x,float y, float r1, float r2, float s1, float s2, float s){
 position= new PVector(x,y);
 this.r1=r1;
 this.r2=r2;
 this.scalar1 = s1;
 this.scalar2 = s2;
 this.speed = s;
 }
 
 void display(){

 stroke(200,210);
    fill(200,210);
    position.x= constant1 + sin(angle) * scalar1; 
    position.y= constant2 + cos(angle) * scalar2;
    vx = speed * scalar1 * cos(angle);
    vy = - speed * scalar2 * sin(angle); 
    angle = angle + speed;
    ellipse(position.x,position.y, r1, r1);
 }
}