//Final 594E
//Interactive Album Cover
//Woohun Joo
//This includes minim library (not compatible with Processing 3 yet)

int screenSize = 600;
int circleCenter = screenSize/2;
int circleRadius = 470;
int bgColor = 43;

//strokeWeight
int strokeC = 3;
int strokeF = 3;
int strokeM = 3;

//key status
boolean keyP = false;

//RGB values of the shadow of the VICTORIUS letter 
float shadowR = 0;
float shadowG = 0;
float shadowB = 0;

//shooting star (I adopted the shooting star part from openprocessing.org)
int[] shootX = new int[30];
int[] shootY = new int[30];
int METEOR_SIZE = 20; // initial size when it first appears
float meteorSize = METEOR_SIZE; // size as it fades
// distance a shooting star moves each frame - varies with each new shooting star
float ssDeltaX, ssDeltaY; 
// -1 indicates no shooting star, this is used to fade out the star
int ssTimer = -1;
// starting point of a new shooting star, picked randomly
int startX, startY;
  
//audio 
import ddf.minim.*;
import ddf.minim.signals.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
Minim minim;
AudioPlayer player;
FFT fft;

float s_volume;
float volume;
float easing = 0.2;

//class
Victo myVicto1;
Victo myVicto2;


void setup(){  
  
  frameRate(120);
  smooth();
  size(screenSize, screenSize, P2D);
  //size(screenSize, screenSize, P3D);
  
  myVicto1 = new Victo(255);
  
  //sound loading
  minim = new Minim(this);
  player = minim.loadFile("wolfmother.mp3");
  fft = new FFT(player.bufferSize(), player.sampleRate());
  fft.logAverages(60, 1);  
}

void draw(){
  background(bgColor,bgColor,bgColor);
  
  get_volume();
  drawCircle();
  drawMt();

  if(key == ' ' && keyP == true){
  shadowR = random(255);
  shadowG = random(255);
  shadowB = random(255);
  
  //shooting star
  for (int i = 0; i < shootX.length-1; i++) {
    int shooterSize = max(0,int(meteorSize*i/shootX.length));
    // to get the tail to disappear need to switch to noStroke when it gets to 0
    if (shooterSize > 0) {
      strokeWeight(shooterSize);
      stroke(255);
    }
    else
      noStroke();
    line(shootX[i], shootY[i], shootX[i+1], shootY[i+1]);
    // ellipse(shootX[i], shootY[i], meteorSize*i/shootX.length,meteorSize*i/shootX.length);
  }
  meteorSize*=0.9; // shrink the shooting star as it fades
  
  // move the shooting star along it's path
  for (int i = 0; i < shootX.length-1; i++) {
    shootX[i] = shootX[i+1];
    shootY[i] = shootY[i+1];
  }
  
  // add the new points into the shooting star as long as it hasn't burnt out
  if (ssTimer >= 0 && ssTimer < shootX.length) {
    shootX[shootX.length-1] = int(startX + ssDeltaX*(ssTimer));
    shootY[shootY.length-1] = int(startY + ssDeltaY*(ssTimer));
    ssTimer++;
    if (ssTimer >= shootX.length) {
      ssTimer = -1; // end the shooting star
    }
  }

  // create a new shooting star with some random probability
  if (random(5) < 1 && ssTimer == -1) {
    newShootingStar();
  }
  
  } else if(key == ' ' && keyP == false){
  shadowR = 0;
  shadowG = 0;
  shadowB = 0;
  
  }
  
  int grad = color(shadowR,shadowG,shadowB); 
  myVicto2 = new Victo(grad);
  
  //color
  translate(0, 60);
  float x = 0;
  for(int y = 0; y > -10; y=y-1) {
    myVicto2.display();
    translate(x, y);
  }
    
  
  hiddenBox();
  
  //white
  shapeMode(CENTER);
  translate(21,25);
  float zooom = map(volume, 0, 5, 1.00, 1.05);
  scale(1,zooom);
  myVicto1.display();
  shapeMode(CORNER);
  

}


//hidden Box
void hiddenBox(){
  rectMode(CENTER);
  noStroke();
  rect(width/2, height/2+257, width, 100);
}

















