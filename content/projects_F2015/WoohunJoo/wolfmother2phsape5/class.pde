//Victorious Class
class Victo {
  //Variables
  PShape v;
  PShape i;
  PShape c;
  PShape t;
  PShape o;
  PShape r;
  PShape ii;
  PShape ou;
  PShape u;
  PShape s;
  
  color cc;

  float posWV1;
  float posWV2;
  float posWV3;
  float posWI1, posWI2, posWI3;
  float posWC1, posWC2;
  float posWT1, posWT2, posWT3;
  float posWO1, posWO2;
  float posWR1, posWR2;
  float posWII1, posWII2, posWII3;
  float posWOO1, posWOO2;
  float posWU1, posWU2;
  float posWS1, posWS2;
  float posH1, posH2, posH3, posH4;
  
  //constructor
  Victo(color tempC){
  cc = tempC;

  posWV1 = width/4-95;
  posWV2 = width/4-75;
  posWV3 = width/4-55;
  
  posWI1 = width/4-45;
  posWI2 = width/4-5;
  posWI3 = width/4-25;
  
  posWC1 = width/3-5;
  posWC2 = width/3-45;
  
  posWT1 = width/3+5;
  posWT2 = width/2-55;
  posWT3 = width/2-75;
  
  posWO1 = width/2-45;
  posWO2 = width/2-5;
  
  posWR1 = width/2+5;
  posWR2 = width/2+45;
  
  posWII1 = width/2+55;
  posWII2 = width/2+75;
  posWII3 = width/2+95;
  
  posWOO1 = width/2+105;
  posWOO2 = width/2+145;
  
  posWU1 = width/2+155;
  posWU2 = width/2+195;
  
  posWS1 = width-55;
  posWS2 = width-95;
  
  posH1 = height/2-20;
  posH2 = height/2+20;
  posH3 = height/2+35;
  posH4 = height/2+5;
  
  //V
  v = createShape();
  v.beginShape();
  v.noFill();
  v.strokeJoin(BEVEL);
  v.stroke(cc);
  v.strokeWeight(strokeF);
  v.vertex(posWV1, posH1);
  v.vertex(posWV1, posH2);
  v.vertex(posWV2, posH3);
  v.vertex(posWV3, posH2);
  v.vertex(posWV3, posH1);
  v.endShape();
  //I
  i = createShape();
  i.beginShape(LINES);
  i.noFill();
  i.strokeJoin(BEVEL);
  i.stroke(cc);
  i.strokeWeight(strokeF);
  i.vertex(posWI1, posH1);
  i.vertex(posWI2, posH1);
  i.vertex(posWI3, posH1);
  i.vertex(posWI3, posH3);
  i.vertex(posWI1, posH3);
  i.vertex(posWI2, posH3);
  i.endShape();
  //C
  c = createShape();
  c.beginShape();
  c.noFill();
  c.strokeJoin(BEVEL);
  c.stroke(cc);
  c.strokeWeight(strokeF);
  c.vertex(posWC1, posH1);
  c.vertex(posWC2, posH1);
  c.vertex(posWC2, posH3);
  c.vertex(posWC1, posH3);
  c.endShape();
  //T
  t = createShape();
  t.beginShape(LINES);
  t.noFill();
  t.strokeJoin(BEVEL);
  t.stroke(cc);
  t.strokeWeight(strokeF);
  t.vertex(posWT1, posH1);
  t.vertex(posWT2, posH1);
  t.vertex(posWT3, posH1);
  t.vertex(posWT3, posH3);
  t.endShape();
  //O
  o = createShape();
  o.beginShape();
  o.noFill();
  o.strokeJoin(BEVEL);
  o.stroke(cc);
  o.strokeWeight(strokeF);
  o.vertex(posWO1, posH1);
  o.vertex(posWO2, posH1);
  o.vertex(posWO2, posH3);
  o.vertex(posWO1, posH3);
  o.endShape(CLOSE);
  
  //R
  r = createShape();
  r.beginShape();
  r.noFill();
  r.strokeJoin(BEVEL);
  r.stroke(cc);
  r.strokeWeight(strokeF);
  r.vertex(posWR1, posH3);
  r.vertex(posWR1, posH1);
  r.vertex(posWR2, posH1);
  r.vertex(posWR2, posH4);
  r.vertex(posWR1, posH4);
  r.vertex(posWR2, posH3);
  r.endShape();
  
  //I
  ii = createShape();
  ii.beginShape(LINES);
  ii.noFill();
  ii.strokeJoin(BEVEL);
  ii.stroke(cc);
  ii.strokeWeight(strokeF);
  ii.vertex(posWII1, posH1);
  ii.vertex(posWII3, posH1);
  ii.vertex(posWII2, posH1);
  ii.vertex(posWII2, posH3);
  ii.vertex(posWII1, posH3);
  ii.vertex(posWII3, posH3);
  ii.endShape();
  
  //O
  ou = createShape();
  ou.beginShape();
  ou.noFill();
  ou.strokeJoin(BEVEL);
  ou.stroke(cc);
  ou.strokeWeight(strokeF);
  ou.vertex(posWOO1, posH1);
  ou.vertex(posWOO2, posH1);
  ou.vertex(posWOO2, posH3);
  ou.vertex(posWOO1, posH3);
  ou.endShape(CLOSE);
  
  //U
  u = createShape();
  u.beginShape();
  u.noFill();
  u.strokeJoin(BEVEL);
  u.stroke(cc);
  u.strokeWeight(strokeF);
  u.vertex(posWU1, posH1);
  u.vertex(posWU1, posH3);
  u.vertex(posWU2, posH3);
  u.vertex(posWU2, posH1);
  u.endShape();
  
  //S
  s = createShape();
  s.beginShape();
  s.noFill();
  s.strokeJoin(BEVEL);
  s.stroke(cc);
  s.strokeWeight(strokeF);
  s.vertex(posWS1, posH1);
  s.vertex(posWS2, posH1);
  s.vertex(posWS2, posH4);
  s.vertex(posWS1, posH4);
  s.vertex(posWS1, posH3);
  s.vertex(posWS2, posH3);
  s.endShape();
  
  
  }
  //function
  void display(){
       translate(0,-2);
       shape(v);
       shape(i);
       shape(c);
       shape(t);
       shape(o);
       shape(r);
       shape(ii);
       shape(ou);
       shape(u);
       shape(s);  
  }

}

