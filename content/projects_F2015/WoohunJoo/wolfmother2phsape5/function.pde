//Mountain
void drawMt(){
  stroke(0, 223, 248);
  strokeWeight(strokeM);
  fill(bgColor, bgColor, bgColor);
  
  float edgeX = map(volume, 0, 5, -50, 50);
  edgeX = constrain(edgeX, -20, 20);
  
  float edgeY = map(volume, 0, 5, -30, 30);
  edgeY = constrain(edgeY, 0, 20);
  
  beginShape(TRIANGLES);
  strokeJoin(BEVEL);
  
  //1
  vertex(width/5-10+edgeX, height/2+170-edgeY);
  vertex(0, height/2+200);
  vertex(width/5+50, height/2+200);
  //1-1
  vertex(width/5-10+edgeX, height/2+170-edgeY);
  vertex(width/5+50, height/2+200);
  vertex(width/5+120, height/2+200);
  //2
  vertex(width/5+100-edgeX, height/2+185-edgeY);
  vertex(width/5+50, height/2+200);
  vertex(width/5+140, height/2+200);
  //3
  vertex(width/2+edgeX, height/2+160-edgeY);
  vertex(width/2-40, height/2+200);
  vertex(width/2+40, height/2+200);
  //3-1
  vertex(width/2+edgeX, height/2+160-edgeY);
  vertex(width/2+40, height/2+200);
  vertex(width/2+70, height/2+200);
  //4
  vertex(width/2+130-edgeX, height/2+160-edgeY);
  vertex(width/2+70, height/2+200);
  vertex(width/2+170, height/2+200);
  //4-1
  vertex(width/2+130-edgeX, height/2+160-edgeY);
  vertex(width/2+170, height/2+200);
  vertex(width, height/2+200);
  //5
  vertex(width-60+edgeX, height/2+140-edgeY);
  vertex(width/2+170, height/2+200);
  vertex(width, height/2+200);
  endShape();
}

//Center Circle
void drawCircle(){
  
  //shapeMode(CENTER);
  float moving = map(volume, 0, 5, 450, 590);
  moving = constrain(moving, 450, 590);
  
  color c = color(255, 0, 140);
  
  ellipseMode(CENTER);
  noFill();
  
  //int decre = 0;
  
   //for (float radius=470; radius>0; radius=radius-10) {
   for(int decre = 470; decre >0; decre = decre-20){
   colorMode(HSB, 360, 100, 100);
   
   stroke(326, 99, 99);
   //tint(255, 128);
   strokeWeight(strokeC);
  
   ellipse(width/2, width/2, moving-decre, moving-decre);
   decre = decre - 10; 
  // }
   }
   colorMode(RGB, 255, 255, 255);
}

//sound detection
void get_volume() {
  
  if(!player.isPlaying())
    volume = 0;
  else {
    fft.forward(player.mix);
    volume = fft.calcAvg(0, 20000); 
  }  
}

//shooting star
void newShootingStar() {
  int endX, endY;
  startX = (int)random(width);
  startY = (int)random(height);
  endX = (int)random(width);
  endY = (int)random(height);
  ssDeltaX = (endX - startX)/(float)(shootX.length);
  ssDeltaY = (endY - startY)/(float)(shootY.length);
  ssTimer = 0; // starts the timer which ends when it reaches shootX.length
  meteorSize = METEOR_SIZE;
  // by filling the array with the start point all lines will essentially form a point initialy
  for (int i = 0; i < shootX.length; i++) {
    shootX[i] = startX;
    shootY[i] = startY;
  }
}

//when spacebar pressed, music plays or stops
void keyPressed(){
  if(key == ' ' && keyP == false){
    player.play();
    keyP = true;
  } else if(key == ' ' && keyP == true){
    player.pause();
    keyP = false;
  }
}
