/*
This a program which generating an galaxy-like visual effect that many white point 
moving along the trajectory of Fibonaci Spiral. First, I drew a fibonaci spiral by 
algorithm. Next, I implemented an effect that one point moving along the trajectory. 
And then, after refferencing to the code written by Michael Pinn on openprocessing.org, 
I finally finished the project. 
*/

ArrayList particles = new ArrayList();
int numPoints = 14;
int[] fibonaci =new int [numPoints];
float x,y,z,px,py,pz;
int index=-1;

void setup(){
  size(1200,1000,P3D);    
}

void draw(){
  index++;
  background(0); 
  if((frameCount % 2) == 0){
    //particles.add(new Particle(new PVector(0,0,0),index,1));
    particles.add(new Particle(new PVector(0,0,0),index,random(0.7,1.3)));
    particles.add(new Particle(new PVector(0,0,0),index,random(0.7,1.3)));
    particles.add(new Particle(new PVector(0,0,0),index,random(0.7,1.3)));
    }
  translate(width/2,height/2);
  rotateX(radians(mouseY/4));
  rotateY(radians(mouseX/4));
 rotateZ(-radians(float(frameCount)));
 for(int i = 0; i < particles.size(); i++){
    Particle p = (Particle) particles.get(i);
    p.draw();
    p.move();
    fill(0);
    noStroke();
    rect(p.lastx,p.lasty,1.4*fibonaci[numPoints-1]-0.6*fibonaci[numPoints-1],1.4*fibonaci[numPoints-1]-0.6*fibonaci[numPoints-1]);
    if(p.loc.x>p.lastx-8&&p.loc.y>p.lasty-8&&p.loc.x<p.lastx+1.4*fibonaci[numPoints-1]-0.6*fibonaci[numPoints-1]&&p.loc.y<p.lasty+1.4*fibonaci[numPoints-1]-0.6*fibonaci[numPoints-1]){
            particles.remove(p);    
   }

  }
}