class Particle{
PVector loc,div;
float x,y,z,px,py,pz,alpha=255,lastx,lasty;
int index;
float radius;
Particle(PVector loc, int index,float radius){
    this.loc = loc;
    this.index=index;
    this.radius=radius*5;
    lastnumber();
  }
  
void draw(){
    stroke(255,alpha);
    strokeWeight(4);
    point(loc.x,loc.y,loc.z);
    
}

void move(){  
  px = 0;
  py = 0;
  pz = 0;

  if(frameCount-index*2<=180){ 
    loc.x=-cos(radians(frameCount-index*2))*radius+radius;
    loc.y=abs(sin(radians(frameCount-index*2))*radius);
   }
   fibonaci[0] = 1;
   fibonaci[1] = 1;
   px=px+fibonaci[0]*radius;//find the ending point of the arc
   py=py+fibonaci[0]*radius;
   px=px+fibonaci[1]*radius;
   py=py-fibonaci[1]*radius; 
   if(frameCount-index*2>180){
    int a = 1;
    for(int i = 2; i < numPoints; i++){
      fibonaci[i] = fibonaci[i-1] + fibonaci[i-2]; 
   //using swith() instead of if() to implement 'break',and the case 0,1,2,3 are for derections
   switch(a){   
    case 0:
   if(radians(frameCount-index*2)>HALF_PI+i/4*TWO_PI&&radians(frameCount-index*2)<PI+i/4*TWO_PI){
     loc.x = -cos(radians(frameCount-index*2))*radius*fibonaci[i]+px;
    loc.y = sin(radians(frameCount-index*2))*radius*fibonaci[i]+py-fibonaci[i]*radius;}
    px=px+fibonaci[i]*radius;
    py=py-fibonaci[i]*radius;
          a=1;
    break;
    
    case 1:
      if(radians(frameCount-index*2)>PI+i/4*TWO_PI&&radians(frameCount-index*2)<HALF_PI+PI+i/4*TWO_PI){
      loc.x = -cos(radians(frameCount-index*2))*radius*fibonaci[i]+px-fibonaci[i]*radius;
      loc.y = sin(radians(frameCount-index*2))*radius*fibonaci[i]+py;}
      a=2;
      px=px-fibonaci[i]*radius;
      py=py-fibonaci[i]*radius;
      break;  
 
    case 2:
    if(radians(frameCount-index*2)>HALF_PI+PI+i/4*TWO_PI&&radians(frameCount-index*2)<TWO_PI+i/4*TWO_PI){
    loc.x = -cos(radians(frameCount-index*2))*radius*fibonaci[i]+px;
    loc.y = sin(radians(frameCount-index*2))*radius*fibonaci[i]+py+fibonaci[i]*radius;}
    px=px-fibonaci[i]*radius;
    py=py+fibonaci[i]*radius;
    a=3;
    break;
  
    case 3: 
     if(radians(frameCount-index*2)<HALF_PI+i/4*TWO_PI&&radians(frameCount-index*2)>i/4*TWO_PI){
     loc.x = -cos(radians(frameCount-index*2))*radius*fibonaci[i]+px+fibonaci[i]*radius;
     loc.y = sin(radians(frameCount-index*2))*radius*fibonaci[i]+py;}
     px=px+fibonaci[i]*radius;
     py=py+fibonaci[i]*radius;
        a=0;
     break;
   }
  }
 alpha-=0.1;}//changing the transparency
}


//because px and py are refreshing all the time, and the last px and py are fixed, so I decided to generate the final px and py outside the loop.
void lastnumber(){
  x = 0;
  y = 0;
  fibonaci[0] = 1;
  fibonaci[1] = 1;
  x=x+fibonaci[0]*radius;
  y=y+fibonaci[0]*radius;
  x=x+fibonaci[1]*radius;
  y=y-fibonaci[1]*radius;
  int a = 1;
  for(int i = 2; i < numPoints; i++){
    fibonaci[i] = fibonaci[i-1] + fibonaci[i-2]; 
    if(a==0){ 
    x=x+fibonaci[i]*radius;
    y=y-fibonaci[i]*radius;
    a=1;
    continue;
    }
    if(a==1){
    x=x-fibonaci[i]*radius;
    y=y-fibonaci[i]*radius;
    a=2;
    continue;
    }
    if(a==2){
    x=x-fibonaci[i]*radius;
    y=y+fibonaci[i]*radius;
    a=3;
    continue;
    }
    if(a==3){
    x=x+fibonaci[i]*radius;
    y=y+fibonaci[i]*radius;
    a=0;
    continue;
    }
  }
   lastx=x;
   lasty=y;
}

}