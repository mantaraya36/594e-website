Title: Workshop 2
Date: 2015-04-09

Write a Python program that asks the user to guess a number between 1 and 100. If the guess is right, the program exists telling the user how many tries it took. If it is wrong, the program will tell the user whether the number is higher or lower.

You will need to use a while loop to keep the program running until the number is correct. Try to use functions within this while loop to make the code clearer.

The execution of the program should look like:
```
Guess my number: 34
Sorry, too low.
Guess my number: 67
Sorry, too high.
Guess my number: 50
Sorry, too high.
Guess my number: 50
Sorry, too low.
Guess my number: 58
Right!
You got it in 5 tries
```

To have the computer generate a random number you can use the code:
```
import random
r = random.randint(1, 100)
```



<!-- Additional Readings: -->
<!-- -------------------- -->
