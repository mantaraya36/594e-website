int rectWidth;
float r1;
float r2;
float r3;
color fillVal = color(r1, r3, r2);
float rotateValue = 0;
import ddf.minim.analysis.*;
import ddf.minim.*;
Minim       minim;
AudioPlayer song;
FFT         fft;
   
void setup() {
  size(1200, 800);
  noStroke();
  background(r1,r3,r2);
  rectWidth = width/2;
  frameRate(1000);
  smooth();  
  minim = new Minim(this);
  //jingle = minim.loadFile("FEQB.mp3", 1024);
 song = minim.loadFile("OKRF.mp3", 1024);
  //song = minim.loadFile("jingle.mp3", 1024);
  song.loop();
  fft = new FFT( song.bufferSize(), song.sampleRate() );
  
}

void draw() {
  r1 = random(0, 255);
  r2 = random(0, 255);
  r3 = random(0, 255);
  fill(r1,r2,r3,10);
  noStroke();
  rect(0,0,width,height);
  noFill();
  stroke(r1,r3,r2,r1);
  strokeWeight(1);
  rotateValue += 1; //the same as rotateValue = rotateValue+1;
  //println(rotateValue);
  fft.forward( song.mix );
  for(int i = 0; i < fft.specSize(); i++)
  {
 
  pushMatrix();
  translate(600,400);
  rotate(rotateValue);
  //ellipseMode(CORNER);
  //triangle(-400,-200,100,400,10,10);
  triangle(fft.getBand(i)*10, 10, 100, 400, 10, 10);
  ellipse(fft.getBand(i)*10, 10, 100, 400);
  //ellipse(-400,0,100,400);
  //triangle(0,-200,400,400,10,10);
  //quad(38, 31, 86, 20, 69, 63, 30, 76);
  //quad(100, 10, 60, 20, 90, 30, 30, 60);
  //ellipse(0,0,400,100);
  popMatrix();}
}

void keyPressed() {
   r1 = random(0, 255);
   r2 = random(0, 255);
   r3 = random(0, 255);
  int keyIndex = -1;
  if (key >= 'A' && key <= 'Z') {
    keyIndex = key - 'A';
  } else if (key >= 'a' && key <= 'z') {
    keyIndex = key - 'a';
  }
  if (keyIndex == -1) {
    // If it's not a letter key, clear the screen
    background(r1, r2, r3, r1);
  } else { 
    // It's a letter key, fill a rectangle
    fill(r1, r2, r3, r1);
    float x = map(keyIndex, 0, 25, 0, width - rectWidth);
    rect(x, 0, rectWidth, height);
  }
  int x = 0;
  if (keyIndex == -1) {
    line(x, 0, x, 100);
    x = x + 1;
  } else {
    loop();
  }
  saveFrame("framecapture######.png");

}





