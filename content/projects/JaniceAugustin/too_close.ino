int thresholdValue = 900; //the threshold for a specific space (needs to be tuned)
int thresholdValue1 = 927;
int thresholdValue2 = 684;
int thresholdValue3 = 791;

int photocellPin0 = A0; //define ANALOGUE pin for Photo resistor
int photocellPin1 = A1; 
int photocellPin2 = A2; 
int photocellPin3 = A3; 

int transistorPin0 = 3; //digital pin for controlling transistor
int transistorPin1 = 4; 
int transistorPin2 = 5; 
int transistorPin3 = 6; 

int counter0 = 0; //This counter needs to be independent for each photocell, and we just rename it so we keep track of each one
int counter1 = 0;
int counter2 = 0;
int counter3 = 0;

int counterMax = 50; //1/10th of a second
int rest = 6000; //rest time in milliseconds

void setup()
{
  Serial.begin(9600); //we only need to initialize Serial.begin once
  pinMode(transistorPin0, OUTPUT);
  pinMode(transistorPin1, OUTPU//ood. Use this line for as many [transistor+muscle wire combo as u like]
  pinMode(transistorPin2, OUTPUT);
  pinMode(transistorPin3, OUTPUT);
}

void loop()
{
////////////////////////////////////////////////////////////////////////////////////////////////////
//CALIBRATION
//  Serial.println(analogRead(photocellPin0)); //USE THIS TO TUNE THE LIGHTING OF THE SPACE. Write the value of the photoresistor to the serial monitor.
////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////
//UNIT 0
  if (analogRead(photocellPin0) < thresholdValue)
  {
    //    Serial.println("Value is below threshold"); //Do something when it's lower than threshold
    digitalWrite(transistorPin0, HIGH);
    counter0++;
  }
  else if (analogRead(photocellPin0) > thresholdValue)
  {
    //    Serial.println("Value is above threshold"); //Do something when it's higher than threshold
    digitalWrite(transistorPin0, LOW);
    //    delay(100);
  }
  //  digitalWrite(transistorPin0, state);
  Serial.println(analogRead(photocellPin0));
  Serial.println("UNIT 0");
  Serial.println(counter0);
  Serial.println("\n");
  if (counter0 > counterMax) {
    digitalWrite(transistorPin0, LOW);
    delay(rest);
    counter0 = 0;
  }
  delay(100); //short delay for faster response to light.
////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////
//UNIT 1
if (analogRead(photocellPin1) < thresholdValue1)
  {
    digitalWrite(transistorPin1, HIGH);
    counter1++;
  }
  else if (analogRead(photocellPin1) > thresholdValue1)
  {
    //    Serial.println("Value is above threshold"); //Do something when it's higher than threshold
    digitalWrite(transistorPin1, LOW);
    //    delay(100);
  }
  //  digitalWrite(transistorPin1, state);
  Serial.println(analogRead(photocellPin1));
  Serial.println("UNIT 1"); 
  Serial.println(counter1);
  Serial.println("\n"); 
  if (counter1 > counterMax) {
    digitalWrite(transistorPin1, LOW);
    delay(rest);
    counter1 = 0;
  }
  delay(100); //short delay for faster response to light.
////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////
////UNIT 2
  if (analogRead(photocellPin2) < thresholdValue2)
  {
    //    Serial.println("Value is below threshold"); //Do something when it's lower than threshold
    digitalWrite(transistorPin2, HIGH);
    counter2++;
  }
  else if (analogRead(photocellPin2) > thresholdValue2)
  {
    //    Serial.println("Value is above threshold"); //Do something when it's higher than threshold
    digitalWrite(transistorPin2, LOW);
    //    delay(100);
  }
  //  digitalWrite(transistorPin1, state);
  Serial.println(analogRead(photocellPin2));
  Serial.println("UNIT 2"); 
  Serial.println(counter2);
  Serial.println("\n"); 
  if (counter2 > counterMax) {
    digitalWrite(transistorPin2, LOW);
    delay(rest);
    counter2 = 0;
  }
  delay(100); //short delay for faster response to light.
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////
////UNIT 3  
  if (analogRead(photocellPin3) < thresholdValue3)
  {
    //    Serial.println("Value is below threshold"); //Do something when it's lower than threshold
    digitalWrite(transistorPin3, HIGH);
    counter3++;
  }
  else if (analogRead(photocellPin3) > thresholdValue3)
  {
    //    Serial.println("Value is above threshold"); //Do something when it's higher than threshold
    digitalWrite(transistorPin3, LOW);
    //    delay(100);
  }
  //  digitalWrite(transistorPin1, state);
  Serial.println(analogRead(photocellPin3));
  Serial.println("UNIT 3"); 
  Serial.println(counter3);
  Serial.println("\n");
  if (counter3 > counterMax) {
    digitalWrite(transistorPin3, LOW);
    delay(rest);
    counter3 = 0;
  }
  delay(100); //short delay for faster response to light.
//////////////////////////////////////////////////////////////////////////////////////////////////////
}

